const Web3 = require('web3');
// const web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/6a46e5036c724df9b197caa8d5b89c4f")); // mainnet
const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/6a46e5036c724df9b197caa8d5b89c4f")); // ropsten
const abi = [ { "constant": false, "inputs": [ { "name": "_name", "type": "string" }, { "name": "_priceInWei", "type": "uint256" }, { "name": "_validityInDays", "type": "uint256" } ], "name": "addSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "cancleSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "depoitFunds", "outputs": [], "payable": true, "stateMutability": "payable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" }, { "name": "_name", "type": "string" }, { "name": "_price", "type": "uint256" }, { "name": "_validityInDays", "type": "uint256" } ], "name": "modifySubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "pauseSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "renewSubscription", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "resumeSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "subscribe", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_amount", "type": "uint256" } ], "name": "withdrawFunds", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_user", "type": "address" }, { "indexed": false, "name": "_amount", "type": "uint256" } ], "name": "FundDeposited", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_user", "type": "address" }, { "indexed": false, "name": "_amount", "type": "uint256" } ], "name": "FundWithdrawn", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "OwnershipTransferred", "type": "event" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" } ], "name": "balance", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "calculateRefund", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "getSubscriptionDetails", "outputs": [ { "name": "", "type": "string" }, { "name": "", "type": "uint256" }, { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "pauseDuration", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "subscriptionExpiry", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "subscriptionRegistry", "outputs": [ { "name": "name", "type": "string" }, { "name": "price", "type": "uint256" }, { "name": "validity", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalAvailableSubscriptions", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "totalUsage", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" } ], "name": "totalUserSubscriptions", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "Users", "outputs": [ { "name": "balance", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "userSubscriptions", "outputs": [ { "name": "SID", "type": "uint256" }, { "name": "subscriptionName", "type": "string" }, { "name": "", "type": "uint8" }, { "name": "purchaseDate", "type": "uint256" }, { "name": "lastPauseDate", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "userSubscriptionState", "outputs": [ { "name": "", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
const address = "0x6659ecf054c241a7b51cb3141eae76cbbe37420a";
const contract = web3.eth.contract(abi).at(address);
var functions = {}


functions.getBalance = (userAddress) => {
    return new Promise((resolve, reject) => {
        contract.balance(userAddress, (err, val) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(val.toNumber())
            }
        })
    });  
}

functions.userSubscriptionState = (userAddress, subscriptionIndex) => {
    return new Promise((resolve, reject) => {
        contract.userSubscriptionState(userAddress, subscriptionIndex, (err, state) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(state.toNumber())
            }
        })
    });  
}

functions.totalUserSubscriptions = (userAddress) => {
    return new Promise((resolve, reject) => {
        contract.totalUserSubscriptions(userAddress, (err, total) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(total.toNumber())
            }
        })
    });  
}

functions.totalUsage = (userAddress, subscriptionIndex) => {
    return new Promise((resolve, reject) => {
        contract.totalUsage(userAddress, subscriptionIndex, (err, total) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(total.toNumber())
            }
        })
    });  
}

functions.pauseDuration = (userAddress, subscriptionIndex) => {
    return new Promise((resolve, reject) => {
        contract.pauseDuration(userAddress, subscriptionIndex, (err, value) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(value.toNumber())
            }
        })
    });  
}

functions.subscriptionExpiry = (userAddress, subscriptionIndex) => {
    return new Promise((resolve, reject) => {
        contract.subscriptionExpiry(userAddress, subscriptionIndex, (err, value) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(value.toNumber())
            }
        })
    });  
}

functions.calculateRefund = (userAddress, subscriptionIndex) => {
    return new Promise((resolve, reject) => {
        contract.calculateRefund(userAddress, subscriptionIndex, (err, value) => {
            if(err) {
                reject(err);
            }
            else {
                resolve(value.toNumber())
            }
        })
    });  
}

functions.userSubscriptions = (userAddress) => {
    let promises = [];
    let sub = {};
    return functions.totalUserSubscriptions(userAddress).then(async total => {
        console.log('total:', total)
        for(var i = 0; await i < total; await i++) {
            let promise = new Promise((resolve, reject) => {
                contract.userSubscriptions(userAddress, i, (err, details) => {
                    if(err) {
                        reject(err);
                    }
                    else {                    
                        // console.log(details);
                        details[0] = details[0].toNumber();
                        details[2] = details[2].toNumber();
                        details[3] = details[3].toNumber();
                        details[4] = details[4].toNumber();

                        sub['SID'] = details[0];
                        sub['name'] = details[1];
                        sub['purchase_date'] = new Date(details[3] * 1000);
                        sub['last_pause_date'] = details[4] != 0 ? new Date(details[4] * 1000) : '';
                        if(details[2] == 0) {
                            sub['state'] = 'expired';
                        }
                        if(details[2] == 1) {
                            sub['state'] = 'paused';
                        }
                        if(details[2] == 2) {
                            sub['state'] = 'active';
                        }
                        
                        // console.log(details);
                        console.log(sub);
                        resolve(sub);
                    }
                })
            });
            promises.push(promise);
        }
        return Promise.all(promises);
    });    
}


module.exports = functions;