pragma solidity ^0.4.24;

// ----------------------------------------------------------------------------
// Safe math
// ----------------------------------------------------------------------------
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b <= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b > 0);
        c = a / b;
    }
}

contract Owned {
    address public owner;

    event OwnershipTransferred(address indexed _from, address indexed _to);

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) public onlyOwner {
        require(_newOwner != address(0x0));
        emit OwnershipTransferred(owner,_newOwner);
        owner = _newOwner;
    }
    
}

contract SubscriptionRegistryContract is Owned{
    
    using SafeMath for uint;
    
    Subscription[] subscriptionRegistry;
    mapping(address => User) Users;
    
    enum subscriptionState { Active, Paused }
    
    struct User {
        uint256 balance;
        userSubscription[] subscriptions;
    }
    
    struct Subscription {
        string name;
        uint256 price;
        uint256 validity;
    }
    
    struct userSubscription {
        uint256 SID;
        uint256 purchaseDate;
        uint256 lastPauseDate;
        uint256 totalPauseDuration;
        subscriptionState state;
    }
    
    event FundDeposited(address indexed _user, uint256 _amount);
    event FundWithdrawn(address indexed _user, uint256 _amount);
    
    
    function depoitFunds() public payable {
        Users[msg.sender].balance = Users[msg.sender].balance.add(msg.value);
        emit FundDeposited(msg.sender, msg.value);
    }
    
    
    function withdrawFunds(uint256 _amount) public {
        require(_amount > 0);
        require(Users[msg.sender].balance >= _amount);
        
        msg.sender.transfer(_amount);
        Users[msg.sender].balance = Users[msg.sender].balance.sub(_amount);
        emit FundWithdrawn(msg.sender, _amount);
    }
    
    
    function balance(address _user) view public returns(uint256){
        return Users[_user].balance;
    }
    
    
    function addSubscription(string _name, uint256 _price, uint256 _validity) public {
        require(_price > 0);
        require(_validity > 0);
        // require(keccak256(_name) != keccak256(""));
        
        Subscription memory sub;
        sub.name = _name;
        sub.price = _price;
        sub.validity = _validity;
        
        subscriptionRegistry.push(sub);
    }
    
    
    function subscribe(uint256 _SID) public {
        require(_SID > 0 && _SID < subscriptionRegistry.length);
        require(balance(msg.sender) >= subscriptionRegistry[_SID].price);
        
        userSubscription memory us;
        us.SID = _SID;
        us.purchaseDate = now;
        us.state = subscriptionState.Active;
        
        Users[msg.sender].subscriptions.push(us);
    }
    
    
    function pauseSubscription(uint256 _SID) public {
        uint256 subscriptionIndex = userSubscriptionIndex(_SID);
        require(Users[msg.sender].subscriptions[subscriptionIndex].SID == _SID, "Invalid subscription ID");
        
        Users[msg.sender].subscriptions[subscriptionIndex].state = subscriptionState.Paused;
        Users[msg.sender].subscriptions[subscriptionIndex].lastPauseDate = now;
    }
    
    
    function resumeSubscription(uint256 _SID) public {
        uint256 subscriptionIndex = userSubscriptionIndex(_SID);
        require(Users[msg.sender].subscriptions[subscriptionIndex].SID == _SID, "Invalid subscription ID");
        require(Users[msg.sender].subscriptions[subscriptionIndex].state == subscriptionState.Paused , "Subscription is already Active");
        
        Users[msg.sender].subscriptions[subscriptionIndex].state = subscriptionState.Active;
        Users[msg.sender].subscriptions[subscriptionIndex].totalPauseDuration = Users[msg.sender].subscriptions[subscriptionIndex].totalPauseDuration.add(now.sub(Users[msg.sender].subscriptions[subscriptionIndex].lastPauseDate));
    }
    
    
    function cancleSubscription(uint256 _SID) public {
        uint256 subscriptionIndex = userSubscriptionIndex(_SID);
        require(Users[msg.sender].subscriptions[subscriptionIndex].SID == _SID, "Invalid subscription ID");
        
    }
    
    
    function userSubscriptionIndex(uint256 _SID) view internal returns(uint256) {
        for(uint256 i = 0; i < Users[msg.sender].subscriptions.length; i++) {
            if(Users[msg.sender].subscriptions[i].SID == _SID)
                return i;
        }
        
        return 0;
    }
    
    
    function totalUsage(uint256 _subscriptionIndex) view public returns(uint256) {
        uint256 interval = now.sub(Users[msg.sender].subscriptions[_subscriptionIndex].purchaseDate);
        uint256 pauseDuration = Users[msg.sender].subscriptions[_subscriptionIndex].totalPauseDuration.add(now.sub(Users[msg.sender].subscriptions[_subscriptionIndex].lastPauseDate));
        
        return interval.sub(pauseDuration);
    }
    
    
    function subscriptionExpiry(uint256 _subscriptionIndex) view public returns(uint256) {
        return subscriptionRegistry[Users[msg.sender].subscriptions[_subscriptionIndex].SID].validity.sub(totalUsage(_subscriptionIndex));
    }
}