import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ContractService } from '../../services/contract.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {

  name;
  price;
  validity;
  account;
  modify;
  currentSID;

  subscriptionList = [];

  constructor(
    private cd: ChangeDetectorRef, 
    private contractService: ContractService
  ) { }

  ngOnInit() {

    this.contractService.getAccounts().subscribe(accounts => {
      this.account = accounts[0];

      this.viewAvailableSubscriptions();
    }, error => {
      alert(error);
    }); 
  }

  addSubscription = () => {
    this.contractService.addSubscription(this.name, this.price * 10 ** 18, this.validity).subscribe(tx => {
      alert('Transaction Created: https://etherscan.io/tx/'+tx);
    })
  }

  viewAvailableSubscriptions = () => {
    this.contractService.totalAvailableSubscriptions().subscribe(async total => {
      for(var i = 0; i < total; i++) {
        await this.contractService.getSubscriptionDetails(i).subscribe(async details => {
          await this.subscriptionList.push(details);
          this.cd.detectChanges();
        })
      }
    })
  }

  onModifyClick = (subscription) => {
    this.modify = true;
    this.name = subscription[0];
    this.price = subscription[1];
    this.validity = subscription[2] / 86400;
    this.currentSID = subscription[3];
    this.cd.detectChanges();
  }

  oncancelModifyClick = () => {
    this.modify = false;
    this.name = null;
    this.price = null;
    this.validity = null;
    this.currentSID = null;
    this.cd.detectChanges();
  }

  modifySubscription = (SID, ) => {
    this.contractService.modifySubscription(this.currentSID, this.name, this.price, this.validity).subscribe( tx => {
      alert('Transaction Created: https://etherscan.io/tx/'+tx);
    }, error => {
      alert(error);
    })
  }

}
