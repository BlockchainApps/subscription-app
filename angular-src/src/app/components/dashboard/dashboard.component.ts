import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ContractService } from '../../services/contract.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  ethAmount;

  stats = [];
  account;
  totalBalance;
  userSubscriptionList = [];
  userSubscriptionStats = {};
  isLoading = true;
  today = Date.now();
  
  constructor(
    private cd: ChangeDetectorRef, 
    private contractService: ContractService
  ) { }

  ngOnInit() {
    this.contractService.getAccounts().subscribe(accounts => {
      this.account = accounts[0];
      this.getStats();
      this.getUserSubscriptions();

      // this.contractService.getSubscriptionDetails(0).subscribe(details => {
      //   console.log(details);
      // })

      // this.contractService.totalAvailableSubscriptions().subscribe(val => {
      //   console.log(val);
      // })
    }, error => {
      alert(error);
    });    
  }

  getStats = () => {
    Observable.forkJoin([
      this.contractService.balance(this.account),
      this.contractService.totalUserSubscriptions(this.account)
    ])
    .subscribe(stats => {
      console.log(stats);
      this.stats = stats;
      this.cd.detectChanges();
    }) 
  }

  getUserSubscriptionStats = (subscriptionIndex) => {
    Observable.forkJoin([
      this.contractService.totalUsage(subscriptionIndex),
      this.contractService.pauseDuration(subscriptionIndex),
      this.contractService.subscriptionExpiry(subscriptionIndex),
      this.contractService.calculateRefund(subscriptionIndex)
    ])
    .subscribe(stats => {
      console.log(stats);
      this.stats = stats;
      this.cd.detectChanges();
    }) 
  }

  depositETH = () => {
    this.contractService.depositETH(this.ethAmount * 10 ** 18).subscribe(tx => {
      alert('Transaction submitted successfully! Txhash: '+tx);
    }, error => {
      alert(error);
    });
  } 

  withdrawETH = () => {
    this.contractService.withdrawETH(this.ethAmount * 10 ** 18).subscribe(tx => {
      alert('Transaction submitted successfully! Txhash: '+tx);
    }, error => {
      alert(error);
    });
  } 

  pauseSubscription = (SID) => {
    this.contractService.pauseSubscription(SID).subscribe(tx => {
      alert('Transaction submitted successfully! Txhash: '+tx);
    }, error => {
      alert(error);
    });
  }
  
  resumeSubscription = (SID) => {
    this.contractService.resumeSubscription(SID).subscribe(tx => {
      alert('Transaction submitted successfully! Txhash: '+tx);
    }, error => {
      alert(error);
    });
  }

  renewSubscription = (SID) => {
    this.contractService.renewSubscription(SID).subscribe(tx => {
      alert('Transaction submitted successfully! Txhash: '+tx);
    }, error => {
      alert(error);
    });
  }

  getUserSubscriptions = () => {
    this.contractService.totalUserSubscriptions(this.account).subscribe(async total => {
      for(var i = 0; i < total; i++) {
        await this.contractService.userSubscriptions(this.account, i).subscribe(async details => {
          console.log(details);
          await this.userSubscriptionList.push(details);

          await Observable.forkJoin([
            this.contractService.totalUsage(details[5]),
            this.contractService.pauseDuration(details[5]),
            this.contractService.subscriptionExpiry(details[5]),
            this.contractService.calculateRefund(details[5])
          ])
          .subscribe(stats => {
            console.log(stats);
            details.push(stats);
            this.cd.detectChanges();
          })

          this.cd.detectChanges();
        })
      }
      this.isLoading = false;
    });
  }
  
}
