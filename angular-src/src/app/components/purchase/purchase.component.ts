import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ContractService } from '../../services/contract.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  account;
  subscriptionList = [];

  constructor(
    private cd: ChangeDetectorRef, 
    private contractService: ContractService
  ) { }

  ngOnInit() {
    this.contractService.getAccounts().subscribe(accounts => {
      this.account = accounts[0];

      this.viewAvailableSubscriptions();
    }, error => {
      alert(error);
    }); 
  }

  viewAvailableSubscriptions = () => {
    this.contractService.totalAvailableSubscriptions().subscribe(async total => {
      for(var i = 0; i < total; i++) {
        await this.contractService.getSubscriptionDetails(i).subscribe(async details => {
          await this.subscriptionList.push(details);
          this.cd.detectChanges();
        })
      }
    })
  }

  subscribe = (SID) => {
    this.contractService.subscribe(SID).subscribe(tx => {
      alert('Transaction Created: https://etherscan.io/tx/'+tx);
    }, error => {
      alert(error);
    })
  }

}
