import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { fromPromise } from 'rxjs/observable/fromPromise';

const Web3 = require('web3');
declare var window: any;


@Injectable()
export class ContractService {

  public web3: any;
  public account: any;
  public contract : any;
  public contractAddress : String;
  public abiArray = [ { "constant": false, "inputs": [ { "name": "_name", "type": "string" }, { "name": "_priceInWei", "type": "uint256" }, { "name": "_validityInDays", "type": "uint256" } ], "name": "addSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "cancleSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "depoitFunds", "outputs": [], "payable": true, "stateMutability": "payable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" }, { "name": "_name", "type": "string" }, { "name": "_price", "type": "uint256" }, { "name": "_validityInDays", "type": "uint256" } ], "name": "modifySubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "pauseSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "renewSubscription", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "resumeSubscription", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "subscribe", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_amount", "type": "uint256" } ], "name": "withdrawFunds", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_user", "type": "address" }, { "indexed": false, "name": "_amount", "type": "uint256" } ], "name": "FundDeposited", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_user", "type": "address" }, { "indexed": false, "name": "_amount", "type": "uint256" } ], "name": "FundWithdrawn", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" } ], "name": "OwnershipTransferred", "type": "event" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" } ], "name": "balance", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "calculateRefund", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_SID", "type": "uint256" } ], "name": "getSubscriptionDetails", "outputs": [ { "name": "", "type": "string" }, { "name": "", "type": "uint256" }, { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "pauseDuration", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "subscriptionExpiry", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "subscriptionRegistry", "outputs": [ { "name": "name", "type": "string" }, { "name": "price", "type": "uint256" }, { "name": "validity", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalAvailableSubscriptions", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "totalUsage", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" } ], "name": "totalUserSubscriptions", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" } ], "name": "Users", "outputs": [ { "name": "balance", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "userSubscriptions", "outputs": [ { "name": "SID", "type": "uint256" }, { "name": "subscriptionName", "type": "string" }, { "name": "", "type": "uint8" }, { "name": "purchaseDate", "type": "uint256" }, { "name": "lastPauseDate", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_user", "type": "address" }, { "name": "_subscriptionIndex", "type": "uint256" } ], "name": "userSubscriptionState", "outputs": [ { "name": "", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" } ];

  constructor() { 
    this.instantiateWeb3AndContract();
  }

  instantiateWeb3AndContract = () => {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      console.warn(
        'Using web3 detected from external source. If you find that your accounts don\'t appear or you have 0 MetaCoin, ensure you\'ve configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask'
      );
      // Use Mist/MetaMask's provider
      // this.web3 = new Web3(window.web3.currentProvider);
      this.web3 = new Web3(window.ethereum); // New Metamask method EIP-1102
      this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress); 
      this.web3.version.getNetwork((err, netId) => {
        switch (netId) {
          case "1":
            this.contractAddress = ""; // mainnet
            this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress);
            break
          case "3":
            this.contractAddress = "0x6659ecf054c241a7b51cb3141eae76cbbe37420a" ; //testnet
            this.contract = this.web3.eth.contract(this.abiArray).at(this.contractAddress);
            console.log(this.contract);
            // alert('Alert: This is a Test Network. Kindly select Main Etherum Network to use the application.' );
            break
          default:
            alert('Error: Please choose Main Ethereum Network to use the application.');
        }
      });
      this.web3.eth.getAccounts((err, accs) => {
        this.account = accs[0];
  	  });
    } else {
      alert('MetaMask not found. Kindly install MetaMask to use this application.');
    }
  };

  getAccounts(): Observable<any> {
  	return Observable.create(observer => {
      
      // New Metamask method EIP-1102
      window.ethereum.enable()
      .then(accs=> {

        if (accs.length === 0) {
  	      observer.error('Your metamask account is locked. Plese unlock account make transactions.')
        }
        
        this.account = accs[0];
        observer.next(accs)
  	    observer.complete()
      })
      .catch( err => {
        observer.error(err);
      })

  	  // this.web3.eth.getAccounts((err, accs) => {
  	  //   if (err != null) {
  	  //     observer.error('There was an error fetching your accounts.')
  	  //   }

  	  //   if (accs.length === 0) {
  	  //     observer.error('Your metamask account is locked. Plese unlock account make transactions.')
  	  //   }

      //   this.account = accs[0];
      //   console.log(accs);
  	  //   observer.next(accs)
  	  //   observer.complete()
  	  // });
  	})
  }

  depositETH (ethAmount) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.depoitFunds({from: this.account, value: ethAmount},(err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  withdrawETH (ethAmount) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.withdrawFunds(ethAmount, {from: this.account}, (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  subscribe (SID) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.subscribe(SID, {from: this.account}, (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  pauseSubscription (SID) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.pauseSubscription(SID, {from: this.account}, (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  resumeSubscription (SID) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.resumeSubscription(SID, {from: this.account}, (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  totalUsage (subscriptionIndex) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.totalUsage(this.account, subscriptionIndex, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  }

  pauseDuration (subscriptionIndex) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.pauseDuration(this.account, subscriptionIndex, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  }

  subscriptionExpiry (subscriptionIndex) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.subscriptionExpiry(this.account, subscriptionIndex, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  }

  calculateRefund (subscriptionIndex) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.calculateRefund(this.account, subscriptionIndex, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  } 

  renewSubscription (SID) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.renewSubscription(SID, {from: this.account}, (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  balance (user) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.balance(user, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  } 

  totalUserSubscriptions (user) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.totalUserSubscriptions(user, (err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  } 

  userSubscriptions (user, index) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.userSubscriptions(user, index, (err, details) => {
        if(err) {
          observer.error(err);
        }
        else {
          details.push(index);
          observer.next(details);
          observer.complete();
        }
      });
  	})
  }  

  addSubscription (name, price, validity) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.addSubscription(name, price, validity, {from: this.account} , (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  modifySubscription (SID, name, price, validity) :  Observable<any> { 
    console.log(SID);
    console.log(name);
    console.log(price);
    console.log(validity);
    return Observable.create( observer => {
  	  this.contract.modifySubscription(SID, name, price, validity, {from: this.account} , (err, tx) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(tx);
          observer.complete();
        }
      });
  	})
  } 

  totalAvailableSubscriptions () :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.totalAvailableSubscriptions((err, value) => {
        if(err) {
          observer.error(err);
        }
        else {
          observer.next(value.toNumber());
          observer.complete();
        }
      });
  	})
  } 

  getSubscriptionDetails (SID) :  Observable<any> { 
    return Observable.create( observer => {
  	  this.contract.getSubscriptionDetails(SID, (err, details) => {
        if(err) {
          observer.error(err);
        }
        else {
          details[1] = details[1].toNumber();
          details[2] = details[2].toNumber();
          details.push(SID);
          observer.next(details);
          observer.complete();
        }
      });
  	})
  }
  
}
