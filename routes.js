const express = require('express');
const router = express.Router();
const contract = require('./functions');


router.get('/balance/:userAddress', (req, res) => {
    contract.getBalance(req.params.userAddress)
    .then(balance => {
        res.status(200).json({balance: balance});
    })
    .catch(error => {
        res.status(400).json({error: error});
    })
});


router.get('/totalSubscriptions/:userAddress', (req, res) => {
    contract.totalUserSubscriptions(req.params.userAddress)
    .then(total => {
        res.status(200).json({total: total});
    })
    .catch(error => {
        res.status(400).json({error: error});
    })
});

router.get('/usersubscriptions/:userAddress', (req, res) => {

    contract.userSubscriptions(req.params.userAddress)
    .then(subscriptions => {
        res.status(200).json(subscriptions);
    })
    .catch(error => {
        res.status(400).json({error: error});
    })
});

module.exports = router;