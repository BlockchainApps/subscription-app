const express       = require('express');
const app           = express();
const path          = require('path');
const routes        = require('./routes');
const port          = process.env.PORT || 8080;

app.use(express.static('public'));
app.use(express.static('uploads'));


// Dist
app.use(express.static(__dirname + '/dist'));
app.use('/api', routes);

// Main 
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

// launch 
app.listen(port);
console.log('Server is running on port:' + port);
